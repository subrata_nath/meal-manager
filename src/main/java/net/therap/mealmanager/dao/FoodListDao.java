package net.therap.mealmanager.dao;

import net.therap.mealmanager.entity.FoodList;
import net.therap.mealmanager.utils.QueryExecutor;
import net.therap.mealmanager.utils.ResultSetProcessor;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author subrata
 * @since 11/13/16
 */
public class FoodListDao {

    public void insertFoodListItems(List<FoodList> targetList) throws SQLException {

        for (FoodList foodList : targetList) {
            QueryExecutor.executeInsertOrUpdateQuery("insert into Food_list (food_name) values(?)",
                    foodList.getFoodName());
        }
    }

    public ArrayList<FoodList> readFoodListItems() throws SQLException {

        ArrayList<FoodList> foodLists;

        foodLists = QueryExecutor.executeSelectQuery("select * from Food_list order by food_id",
                new ResultSetProcessor<FoodList>() {
                    @Override
                    public FoodList getEntityFromResultSet(ResultSet resultSet) throws SQLException {
                        FoodList foodList = new FoodList();
                        foodList.setFoodName(resultSet.getString("food_name"));
                        foodList.setFoodId(resultSet.getInt("food_id"));
                        return foodList;
                    }
                });

        return foodLists;
    }

    public void deleteFoodListItems(List<FoodList> targetList) throws SQLException {
        for (FoodList foodList : targetList) {
            QueryExecutor.executeInsertOrUpdateQuery("delete from Food_list where food_name like ? ",
                    foodList.getFoodName());
        }
    }

    public int readFoodIdByName(String foodname) throws SQLException {
        ArrayList<FoodList> foodLists;
        foodLists = QueryExecutor.executeSelectQuery("select * from Food_list where food_name like ?",
                new ResultSetProcessor<FoodList>() {
                    @Override
                    public FoodList getEntityFromResultSet(ResultSet resultSet) throws SQLException {
                        FoodList foodList = new FoodList();
                        foodList.setFoodName(resultSet.getString("food_name"));
                        foodList.setFoodId(resultSet.getInt("food_id"));
                        return foodList;
                    }
                }, foodname);

        if (foodLists.size() == 1) {
            return foodLists.get(0).getFoodId();
        } else {
            return -1;
        }
    }
}
