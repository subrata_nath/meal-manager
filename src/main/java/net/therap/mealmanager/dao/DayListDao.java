package net.therap.mealmanager.dao;

import net.therap.mealmanager.entity.DayList;
import net.therap.mealmanager.utils.QueryExecutor;
import net.therap.mealmanager.utils.ResultSetProcessor;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * @author subrata
 * @since 11/13/16
 */
public class DayListDao {

    public ArrayList<DayList> readDayList() throws SQLException {
        ArrayList<DayList> dayLists;

        dayLists = QueryExecutor.executeSelectQuery("select * from Day_list order by day_id",
                new ResultSetProcessor<DayList>() {
                    @Override
                    public DayList getEntityFromResultSet(ResultSet resultSet) throws SQLException {
                        DayList dayList = new DayList();
                        dayList.setDayId(resultSet.getInt("day_id"));
                        dayList.setDayName(resultSet.getString("day_name"));
                        return dayList;
                    }
                });

        return dayLists;
    }

    public int getDayIdByName(String dayName) throws SQLException {
        ArrayList<DayList> dayLists;

        dayLists = QueryExecutor.executeSelectQuery("select * from Day_list where day_name like ?",
                new ResultSetProcessor<DayList>() {
                    @Override
                    public DayList getEntityFromResultSet(ResultSet resultSet) throws SQLException {
                        DayList dayList = new DayList();
                        dayList.setDayId(resultSet.getInt("day_id"));
                        dayList.setDayName(resultSet.getString("day_name"));
                        return dayList;
                    }
                }, dayName);

        if (dayLists.size() == 1) {
            return dayLists.get(0).getDayId();
        } else {
            return -1;
        }
    }
}
