package net.therap.mealmanager.dao;

import net.therap.mealmanager.entity.MealType;
import net.therap.mealmanager.utils.QueryExecutor;
import net.therap.mealmanager.utils.ResultSetProcessor;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * @author subrata
 * @since 11/13/16
 */
public class MealTypeDao {

    public ArrayList<MealType> read() throws SQLException {

        ArrayList<MealType> mealTypes;

        mealTypes = QueryExecutor.executeSelectQuery("select * from Meal_type order by type_id",
                new ResultSetProcessor<MealType>() {
                    @Override
                    public MealType getEntityFromResultSet(ResultSet resultSet) throws SQLException {
                        MealType mealType = new MealType();
                        mealType.setTypeId(resultSet.getInt("type_id"));
                        mealType.setTypeName(resultSet.getString("type_name"));
                        return mealType;
                    }
                });
        return mealTypes;
    }

    public int getMealTypeIdByName(String typeName) throws SQLException {

        ArrayList<MealType> mealTypes;

        mealTypes = QueryExecutor.executeSelectQuery("select * from Meal_type where type_name like ?",
                new ResultSetProcessor<MealType>() {
                    @Override
                    public MealType getEntityFromResultSet(ResultSet resultSet) throws SQLException {
                        MealType mealType = new MealType();
                        mealType.setTypeId(resultSet.getInt("type_id"));
                        mealType.setTypeName(resultSet.getString("type_name"));
                        return mealType;
                    }
                }, typeName);

        if (mealTypes.size() == 1) {
            return mealTypes.get(0).getTypeId();
        } else {
            return -1;
        }
    }
}

