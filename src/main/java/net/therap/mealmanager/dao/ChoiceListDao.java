package net.therap.mealmanager.dao;

import net.therap.mealmanager.entity.ChoiceList;
import net.therap.mealmanager.entity.DayList;
import net.therap.mealmanager.entity.FoodList;
import net.therap.mealmanager.entity.MealType;
import net.therap.mealmanager.utils.QueryExecutor;
import net.therap.mealmanager.utils.ResultSetProcessor;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author subrata
 * @since 11/13/16
 */
public class ChoiceListDao {

    public void insertChoiceList(List<ChoiceList> targetList) throws SQLException {

        for (ChoiceList choiceList : targetList) {
            QueryExecutor.executeInsertOrUpdateQuery("insert into Choice_list (food_id, day_id, type_id) " +
                            "values(?, ?, ?)", choiceList.getFoodListFk().getFoodId(),
                    choiceList.getDayListFk().getDayId(), choiceList.getMealTypeFk().getTypeId());
        }
    }

    public void deleteChoiceList(List<ChoiceList> targetList) throws SQLException {

        for (ChoiceList choiceList : targetList) {
            QueryExecutor.executeInsertOrUpdateQuery("delete from Choice_list where food_id=? and day_id=? and " +
                            "type_id=?", choiceList.getFoodListFk().getFoodId(), choiceList.getDayListFk().getDayId(),
                    choiceList.getMealTypeFk().getTypeId());
        }
    }

    public ArrayList<ChoiceList> readFoodItemsInParticularDay(DayList particularDayList) throws SQLException {
        ArrayList<ChoiceList> choiceLists;

        choiceLists = QueryExecutor.executeSelectQuery("select * from Choice_list join Meal_type using (type_id) join" +
                " Food_list using (food_id) join Day_list using (day_id) where day_name =?" +
                " order by type_id,food_id", new ResultSetProcessor<ChoiceList>() {
            @Override
            public ChoiceList getEntityFromResultSet(ResultSet resultSet) throws SQLException {
                ChoiceList choiceList = new ChoiceList();
                DayList dayList = new DayList();
                MealType mealType = new MealType();
                FoodList foodList = new FoodList();

                dayList.setDayId(resultSet.getInt("day_id"));
                dayList.setDayName(resultSet.getString("day_name"));
                mealType.setTypeId(resultSet.getInt("type_id"));
                mealType.setTypeName(resultSet.getString("type_name"));
                foodList.setFoodId(resultSet.getInt("food_id"));
                foodList.setFoodName(resultSet.getString("food_name"));

                choiceList.setDayListFk(dayList);
                choiceList.setMealTypeFk(mealType);
                choiceList.setFoodListFk(foodList);
                return choiceList;
            }
        }, particularDayList.getDayName());

        return choiceLists;
    }
}
