package net.therap.mealmanager.service;

import net.therap.mealmanager.dao.DayListDao;
import net.therap.mealmanager.dao.FoodListDao;
import net.therap.mealmanager.dao.MealTypeDao;
import net.therap.mealmanager.entity.DayList;
import net.therap.mealmanager.entity.FoodList;
import net.therap.mealmanager.entity.MealType;

import java.sql.SQLException;

/**
 * @author subrata
 * @since 11/13/16
 */
public class Manager {

    protected static java.util.ArrayList<DayList> getDayListFromDB() throws SQLException {

        DayListDao dayListDao = new DayListDao();
        return dayListDao.readDayList();
    }

    protected static java.util.ArrayList<MealType> getMealTypeFromDB() throws SQLException {

        MealTypeDao mealTypeDao = new MealTypeDao();
        return mealTypeDao.read();
    }

    protected static java.util.ArrayList<FoodList> getUniversalFoodItemsFromDB() throws SQLException {

        FoodListDao foodListDao = new FoodListDao();
        return foodListDao.readFoodListItems();
    }

}
