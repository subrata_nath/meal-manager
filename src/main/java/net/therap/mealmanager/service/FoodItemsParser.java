package net.therap.mealmanager.service;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author subrata
 * @since 11/15/16
 */
public class FoodItemsParser {

    public ArrayList<String> ParseFoodItems(String newFoodItemNames) {

        Pattern pattern = Pattern.compile("([\\w \\-\\w]*[^,])");
        Matcher matcher = pattern.matcher(newFoodItemNames);
        ArrayList<String> foodList = new ArrayList<>();

        while (matcher.find()) {

            String get = matcher.group();
            foodList.add(get);
        }

        return foodList;
    }
}
