package net.therap.mealmanager.service;

import net.therap.mealmanager.dao.ChoiceListDao;
import net.therap.mealmanager.dao.DayListDao;
import net.therap.mealmanager.dao.FoodListDao;
import net.therap.mealmanager.dao.MealTypeDao;
import net.therap.mealmanager.entity.ChoiceList;
import net.therap.mealmanager.entity.DayList;
import net.therap.mealmanager.entity.FoodList;
import net.therap.mealmanager.entity.MealType;
import net.therap.mealmanager.utils.UI;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author subrata
 * @since 11/15/16
 */
public class DailyFoodItemsService {

    private ArrayList<String> arguments;

    public boolean manipulateCommand(String command) throws SQLException {
        String dayName;
        arguments = parseCommands(command);

        if (arguments.get(0).toLowerCase().equals("show")) {

            dayName = arguments.get(1);
            getParticularDayFoodItems(dayName);

        } else if (arguments.get(0).toLowerCase().equals("ins")) {

            dayName = arguments.get(1);
            ArrayList<ChoiceList> choiceLists = generateQueryChoiceLists();
            insertDailyFoodItems(choiceLists);
            getParticularDayFoodItems(dayName);

        } else if (arguments.get(0).toLowerCase().equals("del")) {

            dayName = arguments.get(1);
            ArrayList<ChoiceList> choiceLists = generateQueryChoiceLists();
            deleteDailyFoodItems(choiceLists);
            getParticularDayFoodItems(dayName);

        } else if (arguments.get(0).toLowerCase().equals("quit")) {

            return false;

        } else {

            UI.showErrorMessage();
        }

        return true;
    }

    private void insertDailyFoodItems(ArrayList<ChoiceList> choiceLists) throws SQLException {

        ChoiceListDao choiceListDao = new ChoiceListDao();
        choiceListDao.insertChoiceList(choiceLists);
    }

    private void deleteDailyFoodItems(ArrayList<ChoiceList> choiceLists) throws SQLException {

        ChoiceListDao choiceListDao = new ChoiceListDao();
        choiceListDao.deleteChoiceList(choiceLists);
    }

    private void getParticularDayFoodItems(String dayName) throws SQLException {

        DayListDao dayListDao = new DayListDao();
        DayList dayList = new DayList();
        dayList.setDayName(dayName);

        ChoiceListDao choiceListDao = new ChoiceListDao();
        ArrayList<ChoiceList> outputFoodList = choiceListDao.readFoodItemsInParticularDay(dayList);

        UI.showParticularDayFoodItems(outputFoodList);
    }

    private ArrayList<ChoiceList> generateQueryChoiceLists() throws SQLException {

        ArrayList<ChoiceList> choiceLists = new ArrayList<>();
        String dayName = arguments.get(1);
        String mealTypeName = arguments.get(2);
        for (int i = 3; i < arguments.size(); ++i) {

            ChoiceList choiceList = new ChoiceList();
            DayList dayList = new DayList();
            MealType mealType = new MealType();
            FoodList foodList = new FoodList();
            String foodName = arguments.get(i);

            DayListDao dayListDao = new DayListDao();
            FoodListDao foodListDao = new FoodListDao();
            MealTypeDao mealTypeDao = new MealTypeDao();

            dayList.setDayId(dayListDao.getDayIdByName(dayName));
            mealType.setTypeId(mealTypeDao.getMealTypeIdByName(mealTypeName));
            foodList.setFoodId(foodListDao.readFoodIdByName(foodName));

            choiceList.setDayListFk(dayList);
            choiceList.setMealTypeFk(mealType);
            choiceList.setFoodListFk(foodList);

            choiceLists.add(choiceList);
        }

        return choiceLists;
    }

    private ArrayList<String> parseCommands(String command) {

        Pattern pattern = Pattern.compile("(\\w*[^ ,])");
        Matcher matcher = pattern.matcher(command);
        ArrayList<String> arguments = new ArrayList<>();

        while (matcher.find()) {
            String get = matcher.group();
            arguments.add(get);
        }

        return arguments;
    }
}
