package net.therap.mealmanager.service;

import net.therap.mealmanager.dao.FoodListDao;
import net.therap.mealmanager.entity.FoodList;
import net.therap.mealmanager.utils.UI;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author subrata
 * @since 11/15/16
 */
public class UniversalFoodItemsService {

    private ArrayList<String> arguments;

    public boolean manipulateCommand(String command) throws SQLException {

        arguments = parseCommands(command);
        ArrayList<FoodList> foodListsLocal;

        if (arguments.get(0).toLowerCase().equals("show")) {
            UI.showUniversalFoodItems();

        } else if (arguments.get(0).toLowerCase().equals("ins")) {
            foodListsLocal = generateQueryFoodLists();
            insertUniversalFoodItems(foodListsLocal);

        } else if (arguments.get(0).toLowerCase().equals("del")) {
            foodListsLocal = generateQueryFoodLists();
            deleteUniversalFoodItems(foodListsLocal);

        } else if (arguments.get(0).toLowerCase().equals("quit")) {
            return false;

        } else {
            UI.showErrorMessage();
        }

        return true;
    }

    private void deleteUniversalFoodItems(ArrayList<FoodList> foodListsLocal) throws SQLException {
        FoodListDao foodListDao = new FoodListDao();
        foodListDao.deleteFoodListItems(foodListsLocal);
    }

    private void insertUniversalFoodItems(ArrayList<FoodList> foodListsLocal) throws SQLException {
        FoodListDao foodListDao = new FoodListDao();
        foodListDao.insertFoodListItems(foodListsLocal);
    }

    private ArrayList<FoodList> generateQueryFoodLists() {
        ArrayList<FoodList> fdLists = new ArrayList<>();

        for (int i = 1; i < arguments.size(); ++i) {
            FoodList fdList = new FoodList();
            fdList.setFoodName(arguments.get(i));
            fdLists.add(fdList);
        }
        return fdLists;
    }


    private ArrayList<String> parseCommands(String command) {
        Pattern pattern = Pattern.compile("(\\w*[^ ,])");
        Matcher matcher = pattern.matcher(command);
        ArrayList<String> arguments = new ArrayList<>();

        while (matcher.find()) {
            String get = matcher.group();
            arguments.add(get);
        }

        return arguments;
    }
}
