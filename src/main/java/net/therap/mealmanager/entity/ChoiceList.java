package net.therap.mealmanager.entity;

/**
 * @author subrata
 * @since 11/13/16
 */
public class ChoiceList {

    private FoodList foodListFk;

    private DayList dayListFk;

    private MealType mealTypeFk;

    public FoodList getFoodListFk() {
        return foodListFk;
    }

    public void setFoodListFk(FoodList foodListFk) {
        this.foodListFk = foodListFk;
    }

    public DayList getDayListFk() {
        return dayListFk;
    }

    public void setDayListFk(DayList dayListFk) {
        this.dayListFk = dayListFk;
    }

    public MealType getMealTypeFk() {
        return mealTypeFk;
    }

    public void setMealTypeFk(MealType mealTypeFk) {
        this.mealTypeFk = mealTypeFk;
    }

    @Override
    public String toString() {
        return "ChoiceList{" +
                "foodListFk=" + foodListFk +
                ", dayListFk=" + dayListFk +
                ", mealTypeFk=" + mealTypeFk +
                '}';
    }
}
