package net.therap.mealmanager.entity;

/**
 * @author subrata
 * @since 11/13/16
 */
public class DayList {

    private int dayId;

    private String dayName;

    public int getDayId() {
        return dayId;
    }

    public void setDayId(int dayId) {
        this.dayId = dayId;
    }

    public String getDayName() {
        return dayName;
    }

    public void setDayName(String dayName) {
        this.dayName = dayName;
    }

    @Override
    public String toString() {
        return "DayList{" +
                "dayId=" + dayId +
                ", dayName='" + dayName + '\'' +
                '}';
    }
}
