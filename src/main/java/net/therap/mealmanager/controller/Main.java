package net.therap.mealmanager.controller;

import net.therap.mealmanager.utils.UI;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Scanner;

/**
 * @author subrata
 * @since 11/13/16
 */
public class Main extends UI {

    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws SQLException, IOException {

        while (true) {
            showWelcomeScreen();
            showCommands(PARENT_COMMANDS);

            try {
                int chooseCommand = scanner.nextInt();

                if (chooseCommand == 1) {
                    new UniversalFoodItems();
                } else if (chooseCommand == 2) {
                    new DailyFoodItems();
                } else if (chooseCommand == 3) {
                    break;
                }
            } catch (Exception e) {
                showErrorMessage();
                break;
            }
        }
    }
}