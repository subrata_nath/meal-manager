package net.therap.mealmanager.controller;

import net.therap.mealmanager.service.DailyFoodItemsService;
import net.therap.mealmanager.utils.UI;

import java.sql.SQLException;
import java.util.Scanner;

/**
 * @author subrata
 * @since 11/13/16
 */
public class DailyFoodItems {

    public DailyFoodItems() throws SQLException {

        UI.showDayList();
        UI.showMealTypeList();
        UI.showUniversalFoodItems();

        while (true) {

            UI.showCommands(UI.DAILY_FOOD_MANIPULATE_COMMANDS);
            Scanner scanner = new Scanner(System.in);
            String command = scanner.nextLine();

            DailyFoodItemsService dailyFoodItemsService = new DailyFoodItemsService();

            if (dailyFoodItemsService.manipulateCommand(command) != true) {
                break;
            }
        }
    }


}
