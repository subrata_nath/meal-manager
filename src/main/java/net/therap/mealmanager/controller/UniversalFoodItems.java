package net.therap.mealmanager.controller;

import net.therap.mealmanager.dao.FoodListDao;
import net.therap.mealmanager.service.UniversalFoodItemsService;
import net.therap.mealmanager.utils.UI;

import java.sql.SQLException;
import java.util.Scanner;

/**
 * @author subrata
 * @since 11/13/16
 */
public class UniversalFoodItems {

    public UniversalFoodItems() throws SQLException {

        FoodListDao foodListDao = new FoodListDao();
        foodListDao.readFoodListItems();

        UI.showUniversalFoodItems();

        while (true) {

            UI.showCommands(UI.SUB_COMMANDS);
            Scanner scanner = new Scanner(System.in);
            String command = scanner.nextLine();

            UniversalFoodItemsService universalFoodItemsService = new UniversalFoodItemsService();

            if (universalFoodItemsService.manipulateCommand(command) != true) {

                break;
            }
        }

    }


}
