package net.therap.mealmanager.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * @author subrata
 * @since 11/18/16
 */
public class QueryExecutor {
    public static void executeInsertOrUpdateQuery(String query, Object... args) throws SQLException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = MysqlConnection.getConnection();
            preparedStatement = connection.prepareStatement(query);
            mapArgsIntoPreparedStatement(preparedStatement, args);

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            System.out.println("Insertion/deletion/update error.");
        } finally {
            preparedStatement.close();
        }
    }

    private static void mapArgsIntoPreparedStatement(PreparedStatement preparedStatement, Object... args) throws SQLException {
        int argsCount = 1;

        for (Object arg : args) {

            if (arg instanceof Integer) {
                preparedStatement.setInt(argsCount, (Integer) arg);
            } else if (arg instanceof String) {
                preparedStatement.setString(argsCount, (String) arg);
            }

            ++argsCount;
        }
    }

    public static <T> ArrayList<T> executeSelectQuery(String query, ResultSetProcessor<T> resultSetProcessor,
                                                      Object... args) throws SQLException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        ArrayList<T> outputResult = new ArrayList<T>();

        try {
            connection = MysqlConnection.getConnection();
            preparedStatement = connection.prepareStatement(query);
            mapArgsIntoPreparedStatement(preparedStatement, args);

            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                outputResult.add(resultSetProcessor.getEntityFromResultSet(resultSet));
            }

        } catch (SQLException e) {
            System.out.println("Insertion/deletion/update not successfully completed");
        } finally {
            preparedStatement.close();
            resultSet.close();
        }

        return outputResult;
    }
}
