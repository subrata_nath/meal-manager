package net.therap.mealmanager.utils;

import net.therap.mealmanager.entity.ChoiceList;
import net.therap.mealmanager.entity.DayList;
import net.therap.mealmanager.entity.FoodList;
import net.therap.mealmanager.entity.MealType;
import net.therap.mealmanager.service.Manager;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * @author subrata
 * @since 11/14/16
 */
public class UI extends Manager {

    public static final String UNDERLINE = "\n _______________________________________________________________ \n";

    public static final String EXTENDED_UNDERLINE = "\n_____________________________________________________________" +
            "_______________________________________________________________ _______________________________________" +
            "_____________________\n";

    public static final String WELCOME_LABEL = String.format("%45s", "Welcome to Meal Management System");

    public static final String[] PARENT_COMMANDS =
            {"Press 1 : to manipulate with Universal Food Items",
                    "Press 2 : to manipulate with daily Food Items",
                    "Press 3 : quit"};

    public static final String[] SUB_COMMANDS =
            {"To show universal food items: show",
                    "To insert new food items: ins [Food Name separated by comma]",
                    "To delete existing food items: del [Food Name separated by comma]",
                    "To quit: quit"};

    public static final String[] DAILY_FOOD_MANIPULATE_COMMANDS =
            {"To show items in particular day : show [Day Name]",
                    "To insert items in particular day: ins [Day Name] [Meal Type Name] [Food Name separated by comma]",
                    "To delete items in particular day: del [Day Name] [Meal Type Name] [Food Name separated by comma]",
                    "To quit: quit"};

    public static final String ERROR = "-_- -_- -_- Command execution failed -_- -_- -_-";


    public static final String UNIVERSAL_FOOD_LABEL = String.format("%40s", "Universal food list");

    public static ArrayList<DayList> dayLists = new ArrayList<>();

    public static ArrayList<MealType> mealTypes = new ArrayList<>();

    public static ArrayList<FoodList> foodLists = new ArrayList<>();

    public static void showDayList() throws SQLException {
        dayLists = getDayListFromDB();

        System.out.printf("%s%40s%s%20s %10s %30s|\n", UNDERLINE, "Available Days: ", UNDERLINE,
                "Day ID", "|", "Day Name");

        for (DayList dayList : dayLists) {
            System.out.printf("%20d %10s %30s|\n", dayList.getDayId(), "|", dayList.getDayName());
        }

        System.out.printf(UNDERLINE);
    }

    public static void showMealTypeList() throws SQLException {
        mealTypes = getMealTypeFromDB();

        System.out.printf("%s%40s%s%20s %10s %30s|\n", UNDERLINE, "Available Meal Types", UNDERLINE,
                "Meal Type ID", "|", "Meal Type Name");

        for (MealType mealType : mealTypes) {
            System.out.printf("%20d %10s %30s|\n", mealType.getTypeId(), "|", mealType.getTypeName());
        }

        System.out.printf(UNDERLINE);
    }

    public static void showUniversalFoodItems() throws SQLException {
        foodLists = getUniversalFoodItemsFromDB();

        System.out.printf("%s%s%s%20s %10s %30s|\n", UNDERLINE, UNIVERSAL_FOOD_LABEL, UNDERLINE,
                "Food ID", "|", "Food Name");

        for (FoodList foodList : foodLists) {
            System.out.printf("%20d %10s %30s|\n", foodList.getFoodId(), "|", foodList.getFoodName());
        }

        System.out.printf(UNDERLINE);
    }

    public static void showCommands(String[] commands) {
        System.out.println();

        for (String cmd : commands) {
            System.out.println(cmd);
        }

        System.out.print("\nYour Command: ");
    }

    public static void showWelcomeScreen() {

        System.out.println(UNDERLINE + WELCOME_LABEL + UNDERLINE);
    }

    public static void showParticularDayFoodItems(ArrayList<ChoiceList> choiceLists) {
        System.out.printf("%s%100s%s%20s %30s %30s %30s %30s %30s\n", EXTENDED_UNDERLINE, "Daily food items preview",
                EXTENDED_UNDERLINE, "Meal Type ID", "Meal Type", "Day Id", "Day Name", "Food ID", "Food Name");
        int prevTypeId = 0;

        for (ChoiceList choiceList : choiceLists) {

            if (prevTypeId != choiceList.getMealTypeFk().getTypeId()) {
                System.out.println(EXTENDED_UNDERLINE);
            }

            System.out.printf("%20d %30s %30s %30s %30s %30s\n", choiceList.getMealTypeFk().getTypeId(),
                    choiceList.getMealTypeFk().getTypeName(), choiceList.getDayListFk().getDayId(),
                    choiceList.getDayListFk().getDayName(), choiceList.getFoodListFk().getFoodId(),
                    choiceList.getFoodListFk().getFoodName());

            prevTypeId = choiceList.getMealTypeFk().getTypeId();
        }

        System.out.printf(EXTENDED_UNDERLINE);
    }

    public static void showErrorMessage() {
        System.err.println(ERROR);
    }

    public static void showInsertErrorMessage() {
        System.err.println("Insertion failed");
    }

    public static void showDeleteErrorMessage() {
        System.err.println("Deletion failed");
    }

}