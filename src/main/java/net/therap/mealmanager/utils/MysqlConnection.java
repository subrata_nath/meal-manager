package net.therap.mealmanager.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * @author subrata
 * @since 11/18/16
 */
public class MysqlConnection {
    private static final String CONNECTION_URL = "jdbc:mysql://localhost:3306/Food_management";
    private static final String USER = "root";
    private static final String PASSWORD = "mytherap";

    public static Connection connection;

    public static Connection getConnection() {
        if (connection == null) {
            try {
                connection = DriverManager.getConnection(CONNECTION_URL, USER, PASSWORD);
                System.out.println("SUCCESSS");
            } catch (Exception e) {
                System.out.println("Connection Failed! Check output console");
            }
        }
        return connection;
    }

    public static void closeConnection() {
        try {
            connection.close();
        } catch (SQLException e) {
            System.out.println("Error in closing connection");
        }
    }
}
