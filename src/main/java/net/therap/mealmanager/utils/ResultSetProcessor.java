package net.therap.mealmanager.utils;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author subrata
 * @since 11/18/16
 */
public interface ResultSetProcessor<T> {
    T getEntityFromResultSet(ResultSet resultSet) throws SQLException;
}
